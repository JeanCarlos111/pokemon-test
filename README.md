# Pokémon Test App

## Descripción
Esta es una aplicación web para administrar datos de Pokémon, incluyendo sus nombres, tipos y evoluciones.

## Instalación

### Paso 1: Clonar el repositorio
Clona este repositorio en tu máquina local:

git clone https://gitlab.com/JeanCarlos111/pokemon-test.git
cd pokemon-test

### Paso 2: Instalar dependencias
Asegúrate de tener Node.js instalado. Luego, instala las dependencias necesarias:

npm install

### Paso 3: Iniciar el servidor JSON para Pokémon
Antes de iniciar la aplicación, asegúrate de tener el servidor JSON local en funcionamiento para proporcionar los datos de Pokémon. Abre una terminal y navega hasta la raíz del proyecto. Luego, inicia el servidor JSON:

json-server -w pokemon.json -p 3001

Esto iniciará un servidor local en `http://localhost:3001` que proporcionará los datos de Pokémon desde el archivo `pokemon.json`.

### Paso 4: Iniciar la aplicación

#### Usando live-server
Para ejecutar la aplicación Lit usando live-server, asegúrate de estar en el directorio raíz del proyecto y ejecuta:

live-server

Esto iniciará la aplicación en tu navegador predeterminado. Si no se abre automáticamente, abre tu navegador y navega a `http://localhost:8080`.

Si no, ejecuta directamente el `index.html`.

## Uso

Una vez que la aplicación esté en funcionamiento, podrás ver una lista de Pokémon y sus detalles. Puedes editar los nombres y tipos de las evoluciones de cada Pokémon seleccionado, y se te notificará si intentas editar un nombre de Pokémon que ya está en uso.

## Contribuciones

¡Las contribuciones son bienvenidas! Si deseas contribuir a este proyecto, por favor abre un issue o una pull request en el repositorio.

## Licencia

Este proyecto está licenciado bajo la [Licencia MIT](LICENSE).
