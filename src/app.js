import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@3/core/lit-core.min.js';
import './components/pokemon-list.js';
import './components/pokemon-detail.js';
import './components/edit-pokemon.js';
import './components/modal.js'; 

class PokemonApp extends LitElement {
  static properties = {
    showList: { type: Boolean },
    selectedPokemon: { type: Object },
  };

  static styles = css`
    :host {
      display: block;
      font-family: Arial, sans-serif;
      text-align: center;
    }

    .container {
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      gap: 20px;
      margin-top: 20px;
    }
  `;

  constructor() {
    super();
    this.showList = true;
    this.selectedPokemon = null;
  }

  handlePokemonSelected(event) {
    this.selectedPokemon = event.detail;
    this.showList = false;
  }

  handleGoBack() {
    this.showList = true;
    this.selectedPokemon = null;
  }

  render() {
    return html`
      <div class="container">
        ${this.showList 
          ? html`<pokemon-list 
                    @pokemon-selected=${this.handlePokemonSelected} 
                    .allPokemons=${this.allPokemons}>
                </pokemon-list>`
          : html`
                <pokemon-detail 
                    .pokemon=${this.selectedPokemon} 
                    @go-back=${this.handleGoBack}>
                </pokemon-detail>`
        }
      </div>
    `;
  }
}

customElements.define('pokemon-app', PokemonApp);
