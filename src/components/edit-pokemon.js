import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@3/core/lit-core.min.js';

class EditPokemon extends LitElement {
  static properties = {
    pokemon: { type: Object },
    all: { type: Array },
  };

  static styles = css`
    :host {
      display: block;
      font-family: 'Arial', sans-serif;
      max-width: 600px;
      margin: 0 auto;
      padding: 20px;
      background-color: #fff;
      border: 1px solid #ccc;
      border-radius: 8px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    form {
      display: grid;
      gap: 10px;
    }

    label {
      display: block;
      margin-bottom: 5px;
    }

    input[type="text"], input[type="checkbox"] {
      width: 100%;
      padding: 8px;
      font-size: 16px;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }

    input[type="checkbox"] {
      margin-top: 5px;
    }

    h3 {
      margin-bottom: 10px;
      color: #e62117;
    }
  `;

  constructor() {
    super();
    this.pokemon = null;
    this.all = [];
  }

  handleCheck() {
    const duplicate = this.all.filter(p => p.name.toLowerCase() === this.pokemon.name.toLowerCase());
    if (duplicate.length > 1) {
      this.showModal();
    }
  }

  showModal() {
    const modal = this.shadowRoot.querySelector('modal-info');
    modal.show();
  }

  handleEvolutionNameChange(evolutionIndex, event) {
    const newName = event.target.value;
    this.pokemon.evolutions[evolutionIndex].name = newName;
  }

  handleEvolutionTypeChange(evolutionIndex, event) {
    const newType = event.target.value;
    this.pokemon.evolutions[evolutionIndex].type = newType;
  }

  render() {
    this.all = this.pokemon.all;
    this.pokemon = this.pokemon.poke;
    console.log(this.pokemon)

    if (!this.pokemon || !this.pokemon.evolutions) {
      return html`<div>No Pokémon data available</div>`;
    }

    return html`
      <form>
        <label>
          Name: <input type="text" .value=${this.pokemon.name}>
        </label>
        <label>
          Type: <input type="text" .value=${this.pokemon.type}>
        </label>
        <label>
          <input type="checkbox" @change=${this.handleCheck}> Is Repeated
        </label>

        <h3>Evolutions:</h3>
        ${this.pokemon.evolutions.map((evolution, index) => html`
          <div>
            <label>
              Evolution Name: <input type="text" .value=${evolution.name} @input=${(e) => this.handleEvolutionNameChange(index, e)}>
            </label>
            <label>
              Evolution Type: <input type="text" .value=${evolution.type} @input=${(e) => this.handleEvolutionTypeChange(index, e)}>
            </label>
          </div>
        `)}
      </form>
      <modal-info></modal-info>
    `;
  }
}

customElements.define('edit-pokemon', EditPokemon);
