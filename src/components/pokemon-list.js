import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@3/core/lit-core.min.js';

class PokemonList extends LitElement {
  static properties = {
    pokemons: { type: Array },
    selectedPokemon: { type: Object },
    allPokemons: { type: Array },
  };

  static styles = css`
    :host {
      display: block;
      font-family: 'Arial', sans-serif;
    }

    .pokedex-header {
      background-color: #e62117;
      color: white;
      text-align: center;
      padding: 10px;
      margin-bottom: 20px;
      font-size: 1.5rem;
      text-transform: uppercase;
      letter-spacing: 2px;
      border-radius: 5px;
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    }

    ul {
      display: grid;
      grid-template-columns: repeat(3, 1fr);
      gap: 20px;
      padding: 0;
      list-style: none;
    }

    li {
      cursor: pointer;
      text-align: center;
      background-color: #f0f0f0;
      padding: 15px;
      border-radius: 8px;
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
      transition: transform 0.2s ease-in-out;
    }

    li:hover {
      transform: translateY(-5px);
    }

    img {
      width: 100%;
      max-width: 80px;
      height: auto;
      border-radius: 50%;
      border: 2px solid #ccc;
    }
  `;

  constructor() {
    super();
    this.pokemons = [];
    this.selectedPokemon = null;
    this.allPokemons = [];
    this.fetchPokemons();
  }

  async fetchPokemons() {
    try {
      const response = await fetch('http://localhost:3001/pokemon');
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      this.pokemons = await response.json();
      this.allPokemons = [...this.pokemons];
    } catch (error) {
      console.error('Error fetching Pokémon data:', error);
    }
  }

  selectPokemon(pokemon) {
    console.log(this.allPokemons)
    this.dispatchEvent(new CustomEvent('pokemon-selected', { detail: { all: this.allPokemons, detail: pokemon } }));
  }

  render() {
    return html`
      <ul>
        ${this.pokemons.map(pokemon => html`
          <li @click=${() => this.selectPokemon(pokemon)}>
            <img src="${pokemon.image}" alt="${pokemon.name}">
            <p>${pokemon.name}</p>
          </li>
        `)}
      </ul>
    `;
  }
}

customElements.define('pokemon-list', PokemonList);
