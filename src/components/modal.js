import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@3/core/lit-core.min.js';

class ModalInfo extends LitElement {
  static properties = {
    visible: { type: Boolean },
  };

  constructor() {
    super();
    this.visible = false;
  }

  connectedCallback() {
    super.connectedCallback();
    document.addEventListener('keydown', this.handleEscapeKey.bind(this));
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    document.removeEventListener('keydown', this.handleEscapeKey.bind(this));
  }

  show() {
    this.visible = true;
    document.body.style.overflow = 'hidden';
  }

  hide() {
    this.visible = false;
    document.body.style.overflow = '';
  }

  handleEscapeKey(event) {
    if (event.key === 'Escape') {
      this.hide();
    }
  }

  static styles = css`
    .modal-overlay {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5); 
      display: flex;
      justify-content: center;
      align-items: center;
      z-index: 1000;
    }

    .modal {
      background-color: white;
      padding: 20px;
      border-radius: 8px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
      max-width: 80%;
      max-height: 80%;
      overflow: auto; 
    }

    .modal p {
      margin: 0;
    }

    .modal button {
      background-color: #e62117;
      color: white;
      border: none;
      padding: 10px 20px;
      border-radius: 5px;
      cursor: pointer;
      margin-top: 10px;
    }

    .modal button:hover {
      background-color: #b5130c;
    }
  `;

  render() {
    return html`
      ${this.visible ? html`
        <div class="modal-overlay" @click=${this.hide}>
          <div class="modal" @click=${(e) => e.stopPropagation()}>
            <p>El Pokémon está repetido. Puedes cambiarlo en el punto más cercano.</p>
            <button @click=${this.hide}>Close</button>
          </div>
        </div>
      ` : ''}
    `;
  }
}

customElements.define('modal-info', ModalInfo);
