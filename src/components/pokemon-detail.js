import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@3/core/lit-core.min.js';
import './edit-pokemon.js';
import './modal.js';

class PokemonDetail extends LitElement {
  static properties = {
    pokemon: { type: Object },
    all: { type: Array },
  };

  static styles = css`
    .pokemon-detail {
      display: grid;
      grid-template-columns: 1fr 1fr;
      gap: 20px;
      margin-top: 20px;
      align-items: flex-start;
    }

    .pokemon-info {
      display: flex;
      flex-direction: column;
      align-items: center;
      gap: 12px;
    }

    .evolution-card {
      display: flex;
      align-items: center;
      gap: 20px;
      padding: 10px;
      background-color: #f0f0f0;
      border-radius: 8px;
      box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px;
      margin-bottom: 10px;
      width: 100%;
      justify-content: space-between;
    }

    .evolution-card img {
      width: 100px;
      height: auto;
      border-radius: 8px;
    }

    .evolution-details {
      display: flex;
      flex-direction: column;
      gap: 5px;
    }

    .evolution-details h3 {
      margin: 0;
    }

    button.back-button {
      background-color: red;
      color: white;
      border: none;
      padding: 10px 20px;
      border-radius: 5px;
      cursor: pointer;
      font-size: 16px;
    }

    button.back-button:hover {
      background-color: darkred;
    }

    .edit-pokemon {
      grid-column: 2 / span 1; 
    }
  `;

  constructor() {
    super();
    this.pokemon = null;
    this.all = null;
  }

  goBack() {
    this.dispatchEvent(new CustomEvent('go-back'));
  }

  render() {
    this.all = this.pokemon.all;
    this.pokemon = this.pokemon.detail;

    if (!this.pokemon) {
      return html`<div>No pokemon selected</div>`;
    }

    return html`
      <div class="pokemon-detail">
        <div class="pokemon-info">
          <h2>${this.pokemon.name}</h2>
          <img src="${this.pokemon.image}" alt="${this.pokemon.name}">
          
          ${this.pokemon.evolutions.map(evolution => html`
            <div class="evolution-card">
              <div class="evolution-details">
                <h3>${evolution.name}</h3>
                <p>Type: ${evolution.type}</p>
              </div>
              <img src="${evolution.image}" alt="${evolution.name}">
            </div>
          `)}
          
          <button class="back-button" @click=${this.goBack}>Back</button>
          <modal-info></modal-info>
        </div>
        
        <div class="edit-pokemon">
          <edit-pokemon .pokemon=${{ poke: this.pokemon, all: this.all }}></edit-pokemon>
        </div>
      </div>
    `;
  }
}

customElements.define('pokemon-detail', PokemonDetail);
